# Manual Vagrant Test

* Install virtualbox from https://www.virtualbox.org/wiki/Downloads
* Install vagrant from https://www.vagrantup.com/
* Run `vagrant up` in the main directory to bring up a CentOS 6.7 vm that will provision itself using salt
* Assuming the previous step had no errors, log in to the vm with `vagrant ssh -- -t sudo -i`
* Run `salt-call pillar.items ncexercise` to verify the salt pillar data was set correctly for the port/docroot
* Test whether the salt state is idempotent by running `salt-call --local state.highstate` multiple times
    * Each state that is executed should have an empty *Changes* field in the output
* Go to http://192.168.158.42:8000/ in your local browser, or run `curl http://localhost:8000/` inside the vm
* Run `vagrant destroy` to stop and delete the vagrant vm

# Test using test-kitchen

If you're using RVM and have ruby-2.2.3 installed, then use `gem install bundler` and `bundle install` to
install test-kitchen, kitchen-salt, and the rest of their dependencies.

To test centos 6.7 and 7.1, run `kitchen test`.
