require 'serverspec'
set :backend, :exec

describe package('nginx') do
  it { should be_installed }
end

describe service('nginx') do
  it { should be_enabled }
  it { should be_running }
end

describe port(8000) do
  it { should be_listening }
end

describe file('/var/www/examplesite') do
  it { should be_directory }
  it { should be_owned_by 'nginx' }
  it { should be_grouped_into 'nginx' }
  it { should be_mode 755 }
end

describe file('/var/www/examplesite/index.html') do
  it { should be_file }
  it { should be_owned_by 'nginx' }
  it { should be_grouped_into 'nginx' }
  its(:content) { should match /PSE Exercise/ }
end

describe command('/usr/bin/curl http://localhost:8000/') do
  its(:exit_status) { should eq 0 }
  its(:stdout) { should match /PSE Exercise/ }
end
