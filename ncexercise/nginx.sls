nginx:
  pkg:
    - installed
  service.running:
    - enable: true
    - require:
      - pkg: nginx
      - iptables: fw_nginx_port

/etc/nginx/conf.d/default.conf:
  file.managed:
    - source: salt://ncexercise/nginx_default.conf.jinja
    - template: jinja
    - context:
        docroot: {{ pillar['ncexercise']['docroot'] }}
        port: {{ pillar['ncexercise']['port'] }}
    - require:
      - pkg: nginx
    - watch_in:
      - service: nginx

fw_nginx_port:
  iptables.insert:
    - position: 1
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - dport: {{ pillar['ncexercise']['port'] }}
    - proto: tcp
    - save: True
