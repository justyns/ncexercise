git:
  pkg.installed

puppetlabs_examplesite_docroot:
  file.directory:
    - name: {{ pillar['ncexercise']['docroot'] }}
    - user: nginx
    - group: nginx
    - makedirs: true
    - mode: 755
    - recurse:
        - user
        - group
    - require:
      - pkg: nginx

puppetlabs_examplesite_repo:
  git.latest:
    - name: https://github.com/puppetlabs/exercise-webpage.git
    - target: {{ pillar['ncexercise']['docroot'] }}
    - rev: fcf1520abac4871fcaff88072fed24e1b88a5a96
    - user: nginx
    - require:
      - pkg: git
      - pkg: nginx
      - file: puppetlabs_examplesite_docroot
